import { table } from "https://cdn.skypack.dev/table"

const data = [
  ['S', 'T', 'P', 'H', '*', ' ', '*', 'F', 'P', "L", 'T', 'D'],
  ['S', 'K', 'W', 'R', '*', ' ', '*', 'R', 'B', 'G', 'S', 'Z'],
  [' ', ' ', '#', 'A', 'O', ' ', 'E', 'U', '#', ' ', ' ', ' '],
];
console.log(table(data));
